import { Layout, Menu, MenuProps, theme } from "antd";
import React, { FC, useState } from "react";
import {
  LaptopOutlined,
  NotificationOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Link, Outlet } from "react-router-dom";

const { Header, Content, Sider } = Layout;

interface Props {
  children?: React.ReactNode;
}

const items1: MenuProps["items"] = ["1"].map((key) => ({
  key,
  label: `nav ${key}`,
}));

// const items2: MenuProps["items"] = [
//   UserOutlined,
//   LaptopOutlined,
//   NotificationOutlined,
// ].map((icon, index) => {
//   const key = String(index + 1);

//   return {
//     key: `sub${key}`,
//     icon: React.createElement(icon),
//     label: `subnav ${key}`,

//     children: new Array(4).fill(null).map((_, j) => {
//       const subKey = index * 4 + j + 1;
//       return {
//         key: subKey,
//         label: `option${subKey}`,
//         to: `/dashboard/profilemedia`,
//       };
//     }),
//   };
// });

const menuitem: MenuProps["items"] = [
  {
    key: "sub1",
    label: "Profile",
    icon: <UserOutlined />,
    children: [
      {
        key: "1-1",
        label: <Link to="/dashboard/profiles">Profile Media</Link>,
      },
      {
        key: "1-2",
        label: "Profile Info",
      },
    ],
  },
  {
    key: "sub2",
    label: "subnav 2",
    icon: <LaptopOutlined />,
    children: [
      {
        key: "2-1",
        label: "option5",
      },
      {
        key: "2-2",
        label: "option6",
      },
      {
        key: "2-3",
        label: "option7",
      },
      {
        key: "2-4",
        label: "option8",
      },
    ],
  },
  {
    key: "sub3",
    label: "subnav 3",
    icon: <NotificationOutlined />,
    children: [
      {
        key: "3-1",
        label: "option9",
      },
      {
        key: "3-2",
        label: "option10",
      },
      {
        key: "3-3",
        label: "option11",
      },
      {
        key: "3-4",
        label: "option12",
      },
    ],
  },
];

export const AppLayout: FC<Props> = ({ children }) => {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Header className="header">
        <div className="logo" />
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={["1"]}
          items={items1}
        />
      </Header>
      <Layout>
        <Sider
          onCollapse={setCollapsed}
          collapsible
          collapsed={collapsed}
          collapsedWidth={64}
          width={180}
          style={{ background: colorBgContainer }}
        >
          <Menu
            mode="inline"
            defaultSelectedKeys={["1"]}
            defaultOpenKeys={["sub1"]}
            style={{ height: "100%", borderRight: 0 }}
            items={menuitem}
          />
        </Sider>
        <Layout style={{ padding: "0 24px 24px" }}>
          <Content
            style={{
              padding: 24,
              margin: 24,
              minHeight: 280,
              background: colorBgContainer,
            }}
          >
            <div id="detail">
              <Outlet />
            </div>
          </Content>
        </Layout>
      </Layout>
    </Layout>
  );
};
