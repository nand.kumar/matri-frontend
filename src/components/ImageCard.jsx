import React from 'react';
import { Button, Card } from 'antd';
import { useParams } from 'react-router-dom';
import { approveOrRejectImage } from 'src/data/ProfilesData';

const ImageCard = ({ image, fetchData }) => {
    let { userId } = useParams();
    const handleImageAction = (status) => {
        console.log(userId, image.imageId, status)
        approveOrRejectImage(userId, image.imageId, status);
        fetchData();
    }


    return (

        <Card
            style={{ width: 300, margin: 10 }}
            cover={
                <img
                    alt="example"
                    src={image.imageUrl}
                />
            }
            actions={[
                <Button type='primary' onClick={() => handleImageAction(true)} >Approve</Button>,
                <Button type='primary' danger onClick={() => handleImageAction(false)} >Reject</Button>
            ]}
        >
        </Card>
    )
};

export default ImageCard;