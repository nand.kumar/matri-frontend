export const ProfilesData = [
    {
        name: "Nand",
        profileId: "2",
        images: [
            {
                imageId: "1",
                imageUrl: "https://randomuser.me/api/portraits/men/76.jpg",
                pendingForAction: true,
                isApproved: false
            },
            {
                imageId: "2",
                imageUrl: "https://randomuser.me/api/portraits/men/46.jpg",
                pendingForAction: true,
                isApproved: false
            },
            {
                imageId: "3",
                imageUrl: "https://randomuser.me/api/portraits/men/56.jpg",
                pendingForAction: true,
                isApproved: false
            },
            {
                imageId: "4",
                imageUrl: "https://randomuser.me/api/portraits/men/66.jpg",
                pendingForAction: true,
                isApproved: false
            }

        ],
    },
    {
        name: "Nittin",
        profileId: "1",
        images: [
            {
                imageId: "5",
                imageUrl: "https://randomuser.me/api/portraits/men/12.jpg",
                pendingForAction: true,
                isApproved: false
            },
            {
                imageId: "6",
                imageUrl: "https://randomuser.me/api/portraits/men/22.jpg",
                pendingForAction: true,
                isApproved: false
            },
            {
                imageId: "7",
                imageUrl: "https://randomuser.me/api/portraits/men/25.jpg",
                pendingForAction: true,
                isApproved: false
            },
            {
                imageId: "8",
                imageUrl: "https://randomuser.me/api/portraits/men/28.jpg",
                pendingForAction: true,
                isApproved: false
            },
            {
                imageId: "9",
                imageUrl: "https://randomuser.me/api/portraits/men/15.jpg",
                pendingForAction: true,
                isApproved: false
            },
            {
                imageId: "10",
                imageUrl: "https://randomuser.me/api/portraits/men/59.jpg",
                pendingForAction: true,
                isApproved: false
            }

        ],
    },
    {
        name: "Nittin",
        profileId: "3",
        images: [
            {
                imageId: "5",
                imageUrl: "https://randomuser.me/api/portraits/men/12.jpg",
                pendingForAction: false,
                isApproved: false
            },
            {
                imageId: "6",
                imageUrl: "https://randomuser.me/api/portraits/men/22.jpg",
                pendingForAction: true,
                isApproved: false
            },
            {
                imageId: "7",
                imageUrl: "https://randomuser.me/api/portraits/men/25.jpg",
                pendingForAction: true,
                isApproved: false
            },
            {
                imageId: "8",
                imageUrl: "https://randomuser.me/api/portraits/men/28.jpg",
                pendingForAction: false,
                isApproved: false
            },

        ],
    },

]

export const getProfileWithPendingPhotos = () => {
    const profileWithPendingPhotos = ProfilesData.filter((profile) => {
        const pendingImages = profile.images.filter((image) => image.pendingForAction === true);
        if (pendingImages.length) return true;
        return false;
    })
    return profileWithPendingPhotos;
}


export const getProfile = (profileId) => {
    const profile = ProfilesData.find(profile => profile.profileId === profileId);
    return profile;
}


export const approveOrRejectImage = (profileId, imageId, status) => {
    console.log(profileId, imageId, status)
    const profileIndex = ProfilesData.findIndex(profile => profile.profileId === profileId);
    const imageIndex = ProfilesData[profileIndex].images.findIndex(image => image.imageId === imageId);
    console.log(ProfilesData[profileIndex].images[imageIndex]);
    ProfilesData[profileIndex].images[imageIndex].pendingForAction = false;
    ProfilesData[profileIndex].images[imageIndex].isApproved = status;
}

