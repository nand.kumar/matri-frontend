import { Row, Col } from 'antd'
import React, { useEffect, useState } from 'react'
import ImageCard from 'src/components/ImageCard'
import { Typography } from 'antd';
import { getProfile } from 'src/data/ProfilesData';
import { useParams } from 'react-router-dom';
import { Image } from 'src/components/ImageCard';
const { Title } = Typography;
const User = () => {
    let { userId } = useParams();
    console.log("Id", userId)

    const [images, setImages] = useState([]);
    const [name, setName] = useState("");
    const fetchData = () => {
        const profile = getProfile(userId);
        setName(profile.name)
        console.log(profile)
        const pendingImages = profile.images.filter((image) => image.pendingForAction === true);
        setImages(pendingImages);
    }


    useEffect(() => {
        fetchData();

    }, [])


    return (
        <div>
            <Title style={{ textAlign: 'center' }}>{name} pending photos</Title>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                {
                    images.map((image) => (
                        <Col className="gutter-row" span={8} key={image.imageId}>
                            <ImageCard image={image} fetchData={fetchData} />
                        </Col>
                    ))
                }
            </Row>
        </div>
    )
}

export default User