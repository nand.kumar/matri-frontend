import { getProfileWithPendingPhotos } from '../data/ProfilesData'
import { Space, Table, Typography } from 'antd';
import { Link } from 'react-router-dom';

const { Text } = Typography;
const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Pending Photos',
        dataIndex: 'pendingPhotos',
        key: 'pendingPhotos',
        render: (_, { images }) => {

            const pendingImages = images.filter((image) => image.pendingForAction === true);
            return (

                <Space size="middle">
                    <Text>{pendingImages.length} pending images</Text>
                </Space>
            )
        }
    },
    {
        title: 'View Photos',
        dataIndex: 'viewPhotos',
        key: 'viewPhotos',
        render: (_, { profileId }) => {


            return (
                <Link to={profileId} component={Typography.Link} > View Photos</Link>
            )
        }
    },


];


const Profiles = () => {

    const data = getProfileWithPendingPhotos();
    return (

        <Table columns={columns} dataSource={data} />
    )
}

export default Profiles