import React from 'react';
import './App.css';
import {RouterProvider} from "react-router-dom";
import { AppLayout } from 'src/layout/app.layout';
import {router} from "src/routes/router";


function App() {
  return (

      <RouterProvider router={router}/>
  );
}

export default App;
