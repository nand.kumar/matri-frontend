import {createBrowserRouter} from "react-router-dom";
import {AppLayout} from "src/layout/app.layout";
import {LoginPage} from "src/pages/login.page";
import ErrorPage from "src/routes/error-page";
import Profiles from '../pages/profiles.page';
import User from '../pages/user.page';

const router = createBrowserRouter([
    {
        path: "/",
        element: <div>Hello world!</div>,
        errorElement: <ErrorPage/>,
    },
    {
        path: "/login",
        element: <LoginPage/>
    },

    {
        path: "/dashboard",
        element: <AppLayout/>,
        errorElement: <ErrorPage/>,
        children: [
            {
                path: "",
                element: <div>Dashboard Index</div>
            }
        ]
    },
    {
        path: "/dashboard/profiles",
        element: <AppLayout/>,
        errorElement: <ErrorPage/>,
        children: [
            {
                path: "",
                element: <Profiles/>
            }
        ]
    },
    {
        path: "/dashboard/profiles/:userId",
        element: <AppLayout/>,
        errorElement: <ErrorPage/>,
        children: [
            {
                path: "",
                element: <User/>
            }
        ]
    },
]);

export {router}
